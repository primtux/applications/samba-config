#!/bin/bash

####################################################
# Crée et configure un répertoire de partage samba #
# Auteur : Philippe Ronflette                      #
# philippe.dpt35@yahoo.fr                          #
####################################################

confsamba="/etc/samba/smb.conf"

smbconf="[global]
   workgroup =  WORKGROUP
   server string = %h server
   netbios name = Nom-du-partage
   dns proxy = no
   log file = /var/log/samba/log.%m
   max log size = 1000
   syslog = 0
   panic action = /usr/share/samba/panic-action %d
   security = user
   encrypt passwords = true
   passdb backend = tdbsam
   obey pam restrictions = yes
   unix password sync = yes
   passwd program = /usr/bin/passwd %u
   passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .
   pam password change = yes
   map to guest = bad user
   usershare allow guests = yes

[Nom-du-partage]
   path =/chemin/partage/reseau
   browsable = yes
   writable = yes
   public = yes
"
# Sauvegarde le fichier de configuration original
if [ -e "$confsamba" ] && [ ! -e "$confsamba.orig" ];then
  mv "$confsamba" "$confsamba.orig"
fi

if ! choixrep=$(yad --width=500 --height=100 --title="Configure un partage samba" \
  --image="/usr/share/icons/gnome/32x32/actions/view-restore.png" \
  --form \
    --field "Répertoire à partager:CDIR" /media \
    --field "Nom du partage")
  then exit
fi

cheminrep=$(echo "$choixrep" | cut -d'|' -f 1)
nomrep=$(echo "$choixrep" | cut -d'|' -f 2)
racinerep=$(echo "$cheminrep" | cut -d'/' -f 2)

if [ "$racinerep" != "media" ] || [ "$cheminrep" = "/media" ];then
  danger="true"
  while [ $danger = "true" ];do
    zenity --warning --text "<b>Par sécurité, le répertoire partagé ne peut être\nqu'au sein du répertoire /media,\net ne peut pas être la racine de ce répertoire.</b>" --width="200" --height "80"
    if ! choixrep=$(yad --width=500 --height=100 --title="Configure un partage samba" \
         --form \
          --field "Répertoire à partager:CDIR" /media)
          then exit
    fi
   cheminrep=$(echo "$choixrep" | cut -d'|' -f 1)
   racinerep=$(echo "$cheminrep" | cut -d'/' -f 2)
   if [ "$racinerep" = "media" ] && [ "$cheminrep" != "/media" ];then
     danger="false"
   fi
  done
fi

if [ ! -e "$cheminrep" ];then
  mkdir "$cheminrep"
fi
chmod -R 777 "$cheminrep"

echo "$smbconf" > "$confsamba"
sed -i "s#Nom-du-partage#$nomrep#g;s#path =/chemin/partage/reseau#path=$cheminrep#" "$confsamba"

sudo /etc/init.d/smbd restart
zenity --info --text "<b>Le répertoire de partage samba a été créé avec succès.</b>" --width="200" --height "80"
exit 0


